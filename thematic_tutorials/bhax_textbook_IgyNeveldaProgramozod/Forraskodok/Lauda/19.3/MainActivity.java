package com.example.first;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button press;
    Button exit;
    Button again;
    TextView title;
    TextView scoreText;
    TextView scoreNumber;
    TextView description;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        press = findViewById(R.id.button);
        exit = findViewById(R.id.button2);
        again = findViewById(R.id.button3);
        title = findViewById(R.id.textView);
        scoreText = findViewById(R.id.textView4);
        scoreNumber = findViewById(R.id.textView3);
        description = findViewById(R.id.textView2);
    }

    @SuppressLint("SetTextI18n")
    public void count (View view) {
        counter++;
        scoreNumber.setText(Integer.toString(counter));
    }

    public void exit (View view) {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public void again (View view) {
        counter = 0;
        scoreNumber.setText(String.valueOf(counter));
    }

}
