#include <iostream>
using namespace std;

class Szulo {
public:
	void szulo_uzen()
	{
		cout << "Ez a szulő üzenete\n";
	}
};

class Gyerek : public Szulo {
	void gyerek_uzen()
	{
		cout << "Ez a gyerek üzenete\n";
	}
};

int main()
{
	Szulo * szulo = new Gyerek();
	szulo -> gyerek_uzen();
}
