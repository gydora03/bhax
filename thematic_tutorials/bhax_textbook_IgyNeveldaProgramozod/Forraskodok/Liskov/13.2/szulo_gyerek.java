class Szulo
{
	public static void szulo_uzen()
	{
		System.out.println("Ez a szülő üzenete");
	}	
}

class Gyerek extends Szulo
{
	public static void gyerek_uzen() 
	{
		System.out.println("Ez a gyerek üzenete");
	}
}

public class szulo_gyerek 
{
	public static void main(String[] args) 
	{
		Szulo p = new Gyerek();
		p.gyerek_uzen();
	}
}
