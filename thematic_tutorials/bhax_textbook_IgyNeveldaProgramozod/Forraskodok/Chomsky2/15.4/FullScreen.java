import java.awt.*;
import java.awt.event.*;

public class FullScreen extends Frame implements WindowListener,ActionListener {

        GraphicsDevice gd;    
        TextField text = new TextField(25);
        Button b;
        private int numClicks = 0;	

        public FullScreen(String title) 
        {            
            GraphicsEnvironment ge=GraphicsEnvironment.getLocalGraphicsEnvironment();
            gd=ge.getDefaultScreenDevice();
            setLayout(new FlowLayout());
            addWindowListener(this);
            b = new Button("Click me");
            add(b);
            add(text);
            b.addActionListener(this);
        }

        public void setFullScreen(Frame win)
        {
            win.setUndecorated(true);
            win.setResizable(false);
            gd.setFullScreenWindow(win);
        }

        public void actionPerformed(ActionEvent e) {
            numClicks++;
            text.setText("Button Clicked " + numClicks + " times");
        }

        public void windowClosing(WindowEvent e) {
            dispose();
            System.exit(0);
        }

        public static void main(String[] args) {
            FullScreen myWindow = new FullScreen("My first window");
            myWindow.setFullScreen(myWindow);
        }

}
