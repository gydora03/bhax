#include <exception>
using namespace std;

#ifndef __Csomopont_h__
#define __Csomopont_h__

class Csomopont;

class Csomopont
{
	private: char _betu;
	private: Csomopont* _balNulla;
	private: Csomopont* _jobbEgy;

	public: Csomopont(char aB = '/');

	public: void _Csomopont();

	public: Csomopont* nullasGyermek();

	public: Csomopont* egyesGyermek();

	public: void ujNullasGyermek(Csomopont* aGy);

	public: void ujEgyesGyermek(Csomopont* aGy);

	public: char getBetu();

	private: Csomopont(const Csomopont& aUnnamed_1);

	private: Csomopont& _(const Csomopont& aUnnamed_1);
};

#endif
