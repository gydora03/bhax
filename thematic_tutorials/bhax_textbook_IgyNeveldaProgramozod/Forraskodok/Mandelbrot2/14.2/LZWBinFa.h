#include <exception>
using namespace std;

#ifndef __LZWBinFa_h__
#define __LZWBinFa_h__

#include "Csomopont.h"

class Csomopont;
class LZWBinFa;

class LZWBinFa
{
	private: Csomopont* _fa;
	private: int _melyseg;
	private: int _atlagosszeg;
	private: int _atlagdb;
	private: double _szorasosszeg;
	protected: Csomopont _gyoker;
	protected: int _maxMelyseg;
	protected: double _atlag;
	protected: double _szoras;

	public: LZWBinFa();

	public: void _LZWBinFa();

	public: LZWBinFa(const LZWBinFa& aRegi);

	public: LZWBinFa(LZWBinFa&& aRegi);

	public: LZWBinFa& _<(char aB);

	public: void kiir();

	public: int getMelyseg();

	public: double getAtlag();

	public: double getSzoras();

	public: void kiir(std::ostream& aOs);

	private: void kiir(Csomopont* aElem, std::ostream& aOs);

	private: void szabadit(Csomopont* aElem);

	private: Csomopont* masol(Csomopont* aElem, Csomopont* aRegifa);

	protected: void rmelyseg(Csomopont* aElem);

	protected: void ratlag(Csomopont* aElem);

	protected: void rszoras(Csomopont* aElem);
};

#endif
