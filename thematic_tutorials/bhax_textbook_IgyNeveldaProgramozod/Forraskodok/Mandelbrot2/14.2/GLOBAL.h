#include <exception>
using namespace std;

#ifndef __GLOBAL_h__
#define __GLOBAL_h__

#include "LZWBinFa.h"

class LZWBinFa;
class GLOBAL;

class GLOBAL
{

	public: void usage();

	public: void fgv(LZWBinFa aBinFa);

	public: int main(int aArgc, char* aArgv[] );
};

#endif
